
# nums1 = Array[1, 2, 3, 4, 5]
# puts nums1.to_s


# # =================================================================================

nums1 = Array[1, 2, 3, 4, 5]
# for i in (0..4) do
#   nums1[i] = nums1[i] * 2
# end
# puts "#{nums1}"


# # # =================================================================================

# (3..4).map do |i|
#   nums1[i] = nums1[i] * 2
# end
# puts nums1.to_s

# # # =================================================================================
# nums2 = Array[1, 2, 3, 4, 5]
# # puts(nums2.each { |ele| ele * 2 })

# arrnew1 = nums2.each { |ele| ele * 2 }
# puts arrnew1.to_s
# # puts 

# # # ==================================================================================

# array = Array[1, 2, 3, 4, 5]
# # puts(array.map { |n| n * 2 })

# arrnew2 = array[1..3].map { |ele| ele * 2 }
# puts arrnew2
# puts
# # # ==================================================================================

# array = ["11", "21", "5"]
# arr = array.map { |n| n.to_i}
# puts arr[0].class
# puts

# # #  Ruby Map Shorthand (map &)


# arrays = ['11','21','5']
# arrs = arrays.map(&:to_i)
# puts arrs[0].class
# puts

# puts ["orange", "apple", "banana"].map(&:class)
# # # ==================================================================================


# def pow(p)
#   n**p
# end
# nums = [1, 2, 3, 4, 5]
# user_ids = nums.map { |n| n.pow(2) }
# puts user_ids

# # # https://dev.to/teekay/idiomatic-ruby-writing-beautiful-code-56ef
# # # https://www.brianstorti.com/understanding-ruby-idiom-map-with-symbol/
# # puts
# # # ===================================================================================

# # numbers = [1, 2, 3, 4, 5]
# # even_numbers = []
# # numbers.each do |i|
# #   if i%2 == 0
# #     even_numbers << i
# #   end
# # end
# # # puts even_numbers


numbers = [1, 2, 3, 4, 5].map { |element| element if element.even? }.compact # [ni, 2, nil, 4, nil]
# even_numbers = numbers.compact # [2, 4]
puts numbers


# # # =======================================================================================

# hash = { 1 => 'ram', 2 => 'raju'}
# p(hash.map { |_k, v| v.size})


# hash = { banana: "30", apple: "20" }
# ha = hash.map { |k,v| [k, v.to_i] }
# puts ha.to_s

# p 123.to_s.chars.map(&:to_i)
